package com.atacankullabci.springbootsampleregistration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootSampleRegistrationApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootSampleRegistrationApplication.class, args);
	}

}
